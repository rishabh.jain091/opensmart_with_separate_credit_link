import Fifo::*;
import Types::*;
import VirtualChannelTypes::*;
import CreditTypes::*;

interface ReverseCreditUnit;
  /* Credit */
  method Action putCredit(CreditSignal credit);
  `ifdef SMART
  method Action putCreditSMART(CreditSignal credit);
  `endif
  method ActionValue#(CreditSignal) getCredit;
endinterface

// TODO
// 2 explore:
// mkPipelineFifo with depth 1
// normal FIFO with depth 2
(* synthesize *)
module mkReverseCreditUnit(ReverseCreditUnit);
  Fifo#(NumVCs, CreditSignal)     creditQueue <- mkPipelineFifo;
  Fifo#(NumVCs, CreditSignal)     smartCreditQueue <- mkPipelineFifo;

  method Action putCredit(CreditSignal credit);
    if(isValid(credit)) begin
      creditQueue.enq(credit);
    end
  endmethod

`ifdef SMART
  method Action putCreditSMART(CreditSignal credit);
    if(isValid(credit)) begin
      smartCreditQueue.enq(credit);
    end
  endmethod
`endif

// Cyele PATH breaks.. !
// creditQueue is a pipeline FIFO.
// putCredit and getCredit can't happen in the same cycle for a empty creditQueue.
// essentially, the number of entries used in creditQueue are either 0 or 1.
// later the enq and deq happens together but for different flits.
  method ActionValue#(CreditSignal) getCredit;
    CreditSignal credit = ?;

    if(creditQueue.notEmpty) begin
      creditQueue.deq;
      credit = creditQueue.first();
    end
    else if(smartCreditQueue.notEmpty) begin
      smartCreditQueue.deq;
      credit = smartCreditQueue.first();
    end
    else begin
      credit = Invalid;
    end

    return credit;
  endmethod

endmodule
