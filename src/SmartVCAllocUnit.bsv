import Vector::*;
import Types::*;
import Fifo::*;
import VCAllocUnitTypes::*;

import CreditReg::*;
import VirtualChannelTypes::*;

interface SmartVCAllocUnit;
  method Bool isInited;
  method Bool hasVC;
  method Bool hasVC2;
  method ActionValue#(VCIdx) getNextVC;

  method Action putFreeVC(VCIdx vc);
endinterface

(* synthesize *)
module mkSmartVCAllocUnit(SmartVCAllocUnit);
  Reg#(Bool)                 inited     <- mkReg(False);
  Reg#(VCIdx)                initCount  <- mkReg(0);

  Fifo#(NumVCs, VCIdx)       freeVCPool <- mkBypassFifo;
//  CreditReg                  creditReg  <- mkCreditReg;

// freeVCPool stores the id of free VCs. Initially (at reset), all VCs are free.
// So, freeVCPool stores the id of all VCs of a input port.
  rule initialize(!inited);
    if(initCount < fromInteger(valueOf(NumVCs))) begin
      freeVCPool.enq(initCount);
      initCount <= initCount+1;
    end
    else begin
      inited <= True;
    end
  endrule

  function Bool hasAvailableVC = freeVCPool.notEmpty;

  method Bool isInited = inited;
//  method Bool hasVC = freeVCPool.notEmpty;
  method Bool hasVC = hasAvailableVC;
  method Bool hasVC2 = hasAvailableVC;

// TODO
// This method is called by DataLink interface.
// When a flit leaves the output port buffer, the new VC where the flit will reside
// is updated by calling this method. The VCAllocUnit dequeues this allocated VC from the
// pool.
// This method is called by someone, who wishes to occupy the VC, and so, the VCid won't be free
  method ActionValue#(VCIdx) getNextVC;
    let vc = freeVCPool.first;
    freeVCPool.deq;
    return vc;
  endmethod

  // For a single flit packet, this method should be called when a flit leaves a VC, so
  // increment the count of free VC or store this VCid in the freeVCPool.
  method Action putFreeVC(VCIdx vc) if(inited);
    freeVCPool.enq(vc);
  endmethod
endmodule
