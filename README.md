# OpenSMART 1.X Bluespec System Verilog Version

This work is a update of the original work available [here](https://github.com/hyoukjun/OpenSMART)

If you have installed Bluespec System Verilog compiler you can use the makefile as described
below to either simulate the network or generate verilog and perform fpga synthesis.

## For simulation
set the following variables in the Makefile:
TOP_MODULE:=mkTestBench
TOP_FILE:=TestBench.bsv
TOP_DIR:=testbench
``` bash
make generate_verilog link_verilator simulate
```

## For FPGA synthesis
set the following variables in the Makefile:
TOP_MODULE:=mkNetwork
TOP_FILE:=Network.bsv
TOP_DIR:=src

``` bash
make generate_verilog fpga_build
```

## Other commands:
``` bash
 			Makefile for OpenSmart
help                                              This help dialog.
compile_bsim                                      compile and link the code for BSIM simulations
generate_verilog                                  Generete verilog from BSV
link_verilator                                    Generate simulation executable using Verilator
fpga_build                                        Trigger FPGA synthesis for Artix-7 device
clean-bsv                                         clean bsv build director
clean-verilog                                     delete verilog folder
clean-fpga                                        delete fpga_prject and journal/log files as well
restore                                           clean bsv-build, verilog and fpga folders

```

### Using Logger:
* Follow  : https://gitlab.com/shaktiproject/common_bsv
* Verilator executable is 'sim' file. Example : 
```bash
$ ./bin/sim +mtfg +l1
```

---

* How to config a network?
./src/Types/Types.bsv defines the dimension of network (MeshWidth, MeshHeight), number of VCs (NumUserVCs),
InjectionRate of the synthetic traffic simulation, Routing algorithm (currentRoutingAlgorithm), and so on.

* To implement a network with an arbitrary topology, you need to modify Network.bsv in the 1.0 version.
However, we will provide a DOT file to topology setting converter in a later version.

