## 			Makefile for OpenSmart
include config.inc

TOP_MODULE:=mkTestBench
TOP_FILE:=TestBench.bsv
TOP_DIR:=testbenches
//TOP_MODULE:=mkNetwork
//TOP_FILE:=Network.bsv
//TOP_DIR:=src

#for enabling flit statistics
ifeq ($(detailed_stats), enable)
define_macros += -D DETAILED_STATISTICS=True
endif

# for enabling LFSR traffic generator
ifeq ($(lfsr), enable)
define_macros += -D LFSR=True
endif

# for enabling single cycle router

WORKING_DIR := $(shell pwd)
Vivado_loc=$(shell which vivado)
# ------ synthesis settings ------------#
SYNTHTOP=mkNetwork
JOBS=8
FPGA=xc7a100tcsg324-1
#---------------------------------------#


# ------------------------------------------------------------------------------------------------ #
# ------------------ Include directories for bsv compilation ------------------------------------- #
BSVDEFAULT:=.:%/Prelude:%/Libraries:%/Libraries/BlueNoC
VARIABLE := $(BSVDEFAULT)$(shell sed 's/^/:.\//g' bsvpath)
BSVINCDIR := $(shell echo $(VARIABLE) | sed 's/ //g' )
# ------------------------------------------------------------------------------------------------ #

# ----------------- Setting up flags for verilator ----------------------------------------------- #
THREADS=1
ifeq ($(VERILATESIM), fast)
	verilate_fast := OPT_SLOW="-O3" OPT_FAST="-O3"
endif
VERILATOR_FLAGS += -O3 -LDFLAGS "-static" --x-assign fast --x-initial fast\
--noassert sim_main.cpp --bbox-sys -Wno-STMTDLY -Wno-UNOPTFLAT -Wno-WIDTH \
-Wno-lint -Wno-COMBDLY -Wno-INITIALDLY --autoflush\
-DBSV_RESET_FIFO_HEAD -DBSV_RESET_FIFO_ARRAY -DVERBOSE\
--output-split 20000 --output-split-ctrace 10000
# ----------------- Setting up flags for verilator ----------------------------------------------- #

# ---------------- Setting the variables for bluespec compile  --------------------------------- #
BSVOUTDIR=bin

BSC_VERILOG_CMD:= bsc -u -verilog -elab
BSC_VERILOG_OPTS:= -D simulate +RTS -K40000M -RTS -check-assert  -keep-fires -opt-undetermined-vals \
									$(suppresswarn) -remove-false-rules -remove-empty-rules -remove-starved-rules \
									-remove-dollar -show-schedule -show-module-use -aggressive-conditions
BSC_BSIM_CMD:= bsc -u -sim -elab
BSC_BSIM_OPTS:= +RTS -K40000M -RTS -check-assert  -keep-fires -opt-undetermined-vals \
									$(suppresswarn) -remove-false-rules -remove-empty-rules -remove-starved-rules \
									-show-schedule -show-module-use -aggressive-conditions
BSC_BSIM_LINK:= bsc -u -sim
BSVLINKOPTS:=-parallel-sim-link 8 -keep-fires -Xc++ -O0 -o $(BSVOUTDIR)/sim
VERILOGDIR:=./verilog/
BSVBUILDDIR:=./bsv_build/
ifeq (, $(wildcard ${TOOLS_DIR}/shakti-tools/insert_license.sh))
  VERILOG_FILTER:= -verilog-filter ${BLUESPECDIR}/bin/basicinout \
									 -verilog-filter ./rename_translate.sh
else
  VERILOG_FILTER:= -verilog-filter ${BLUESPECDIR}/bin/basicinout \
									 -verilog-filter ${TOOLS_DIR}/shakti-tools/insert_license.sh \
									 -verilog-filter ./rename_translate.sh
	VERILOGLICENSE:= cp ${TOOLS_DIR}/shakti-tools/IITM_LICENSE.txt ${VERILOGDIR}/LICENSE.txt
endif
# ------------------------------------------------------------------------------------------------ #

# ------------------------------------- Makefile TARGETS ----------------------------------------- #
default: generate_verilog

.PHONY: help
help: ## This help dialog.
	@fgrep -h "##" $(MAKEFILE_LIST) | fgrep -v fgrep | sed -e 's/\\$$//' | sed -e 's/##//' | column	-c2 -t -s :

check-env:
	@if test -z "$$BLUESPECDIR"; then echo "BLUESPECDIR variable not set"; exit 1; fi;

check-py:
	@if ! [ -a /usr/bin/python3 ] ; then echo "Python3 is required in /usr/bin to run AAPG" ; exit 1; fi;


.PHONY: compile_bsim
compile_bsim: ## compile and link the code for BSIM simulations
	@echo Compiling $(TOP_MODULE) in bsim ...
	@mkdir -p $(BSVBUILDDIR);
	@mkdir -p $(VERILOGDIR);
	$(BSC_BSIM_CMD) -bdir $(BSVBUILDDIR) -info-dir $(BSVBUILDDIR)\
  $(BSC_BSIM_OPTS) $(VERILOG_FILTER) \
  -p $(BSVINCDIR) -g $(TOP_MODULE) $(TOP_DIR)/$(TOP_FILE)  || (echo "BSC COMPILE ERROR"; exit 1)
	@mkdir -p $(BSVOUTDIR)
	$(BSC_BSIM_LINK) -bdir $(BSVBUILDDIR) -info-dir $(BSVBUILDDIR) -simdir $(BSVBUILDDIR)\
  $(BSVLINKOPTS) -e $(TOP_MODULE)\
  -p $(BSVINCDIR) || (echo "BSC COMPILE ERROR"; exit 1)

.PHONY: simulate
simulate:
	@echo "Simulating binary: $(BSVOUTDIR)/sim"
	@./$(BSVOUTDIR)/sim

.PHONY: generate_verilog
generate_verilog: ## Generete verilog from BSV
generate_verilog: check-env
	@echo Compiling $(TOP_MODULE) in verilog ...
	@mkdir -p $(BSVBUILDDIR);
	@mkdir -p $(VERILOGDIR);
	$(BSC_VERILOG_CMD) -vdir $(VERILOGDIR) -bdir $(BSVBUILDDIR) -info-dir $(BSVBUILDDIR) $(define_macros) \
  $(BSC_VERILOG_OPTS) $(VERILOG_FILTER) \
  -p $(BSVINCDIR) -g $(TOP_MODULE) $(TOP_DIR)/$(TOP_FILE)  || (echo "BSC COMPILE ERROR"; exit 1)
	@cp ${BLUESPECDIR}/Verilog.Vivado/RegFile.v ${VERILOGDIR}
	@cp ${BLUESPECDIR}/Verilog/FIFO2.v ${VERILOGDIR}
	@cp ${BLUESPECDIR}/Verilog/FIFO1.v ${VERILOGDIR}
	@cp ${BLUESPECDIR}/Verilog/FIFO10.v ${VERILOGDIR}
	@cp ${BLUESPECDIR}/Verilog/FIFOL1.v ${VERILOGDIR}
	@cp ${BLUESPECDIR}/Verilog/Counter.v ${VERILOGDIR}
	@cp ${BLUESPECDIR}/Verilog/SizedFIFO.v ${VERILOGDIR}
	@cp ${BLUESPECDIR}/Verilog/ResetEither.v ${VERILOGDIR}
	@cp ${BLUESPECDIR}/Verilog/MakeReset0.v ${VERILOGDIR}
	@cp ${BLUESPECDIR}/Verilog/SyncReset0.v ${VERILOGDIR}
	@cp ${BLUESPECDIR}/Verilog/ClockInverter.v ${VERILOGDIR}
	@cp ${BLUESPECDIR}/Verilog/SyncFIFO1.v ${VERILOGDIR}
	@cp ${BLUESPECDIR}/Verilog/RevertReg.v ${VERILOGDIR}
	@cp ${BLUESPECDIR}/Verilog/ConstrainedRandom.v ${VERILOGDIR}
	@$(VERILOGLICENSE)
	@echo Compilation finished

.PHONY: link_verilator
link_verilator: ## Generate simulation executable using Verilator
	@echo "Linking $(TOP_MODULE) using verilator"
	@mkdir -p $(BSVOUTDIR) obj_dir
	@echo "#define TOPMODULE V$(TOP_MODULE)" > sim_main.h
	@echo '#include "V$(TOP_MODULE).h"' >> sim_main.h
	verilator $(VERILATOR_FLAGS) --cc $(TOP_MODULE).v -y $(VERILOGDIR) -y common_verilog --exe
	@ln -f -s ../sim_main.cpp obj_dir/sim_main.cpp
	@ln -f -s ../sim_main.h obj_dir/sim_main.h
	#make -j8 -C obj_dir -f V$(TOP_MODULE).mk
	make $(verilate_fast) VM_PARALLEL_BUILDS=1 -j8 -C obj_dir -f V$(TOP_MODULE).mk
	@cp obj_dir/V$(TOP_MODULE) $(BSVOUTDIR)/sim

.PHONY:fpga_build
fpga_build: ## Trigger FPGA synthesis for Artix-7 device
	vivado -nojournal -nolog -mode tcl -notrace -source tcl/create_project.tcl -tclargs $(SYNTHTOP) $(FPGA) \
	|| (echo "Could not create core project"; exit 1)
	vivado -nojournal -log artybuild.log -notrace -mode tcl -source tcl/run.tcl \
		-tclargs $(JOBS) || (echo "ERROR: While running synthesis")

.PHONY: clean-bsv
clean-bsv: ## clean bsv build director
	rm -rf $(BSVBUILDDIR) old_vars $(BSVOUTDIR)

clean-verilog: ## delete verilog folder
clean-verilog:
	rm -rf verilog/

clean-fpga: ## delete fpga_prject and journal/log files as well
	rm -rf fpga_project *.jou *.log

restore: ## clean bsv-build, verilog and fpga folders
restore: clean-bsv clean-verilog clean-fpga

#	@vivado -mode tcl -notrace -source $(SHAKTI_HOME)/src/tcl/create_nexys4_mig.tcl ||\
(echo "Could not create NEXYS4DDR-MIG  IP"; exit 1)
#	@vivado -mode tcl -notrace -source $(SHAKTI_HOME)/src/tcl/create_divider.tcl -tclargs $(XLEN) $(DIVSTAGES) ||\
(echo "Could not create Divider IP"; exit 1)

